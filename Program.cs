﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace ShipmentEntryNumberVerification
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                string cn = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;
                string InputExcelFolder = ConfigurationManager.AppSettings["InputExcelFolder"];
                string SBFolder = ConfigurationManager.AppSettings["SBFolder"];
                string startdate, enddate;
                string startdatetime, enddatetime;
                startdatetime = DateTime.Now.AddDays(-360).ToString();
                enddatetime = DateTime.Now.ToString();

                startdate = Convert.ToDateTime(startdatetime).ToString("yyyy-MM-dd");
                enddate = Convert.ToDateTime(enddatetime).ToString("yyyy-MM-dd");

                //not needed for now
                //ReadInputEntryNumberCsvToDatabase();

                Console.WriteLine("Getting shipment transaction entry number data from {0} to {1}", startdatetime, enddatetime);

                DataTable dtShipment = GetMEVOShipmentTransaction(startdatetime, enddatetime);

                OutputDataTableToAzureDatabase(dtShipment);

                //Environment.Exit(0);

                StringBuilder sb = OutputDataTableToCsvString(dtShipment);

                string reportFolder = ConfigurationManager.AppSettings["OutputReportFolder"];
                string reportFilename = "Shipment-EntryNumber_from_" + startdate + "_to_" + enddate + ".csv";

                File.WriteAllText(reportFolder + "\\" + reportFilename, sb.ToString());

                Console.WriteLine("Ouput report : {0}", reportFolder + "\\" + reportFilename, sb.ToString());

                //SendEmailNotification("New report for Mevo shippment transaction - entry number lookup is generated", "New report file is generated in folder : \n" + reportFolder + "\n\nReport csv file:\n" + reportFolder + "\\" + reportFilename);

            }
            catch(Exception e)
            {
                string err = e.Message;

                Console.WriteLine(err);

                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("ericlee.omnitrans@gmail.com");
                message.To.Add(new MailAddress("elee@omnitrans.com"));
                message.Subject = "ShipmentEntryNumberVerification Execution Error";
                message.IsBodyHtml = false; //to make message body as html  
                message.Body = "ShipmentEntryNumberVerification execution error";
                smtp.Port = 587;
                smtp.Host = "mtlmail2.omni.local"; //for gmail host  
                //smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!!");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);

            }

        }

        public static DataTable GetMEVOShipmentTransaction(string Startdate, string Enddate)
        {
            

            SqlConnection sqlcnn = new SqlConnection();
            sqlcnn.ConnectionString = ConfigurationManager.ConnectionStrings["MTLVPROD3"].ConnectionString;
            sqlcnn.Open();
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = sqlcnn;
            sqlcmd.CommandText = "[MEVOTECH].[ShippingReferenceNumberEntryNumber]";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter para = sqlcmd.Parameters.AddWithValue("@Startdate", Startdate);
            para = sqlcmd.Parameters.AddWithValue("@Enddate", Enddate);
            //para = sqlcmd.Parameters.Add("@IsProcessed", SqlDbType.Bit);
            //sqlcmd.Parameters["@IsProcessed"].Direction = ParameterDirection.Output;
            SqlDataReader sr = sqlcmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(sr);   

            sqlcnn.Close();
            sqlcnn.Dispose();

            return dt;
        }

        public static StringBuilder OutputDataTableToCsvString(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine("\"" + string.Join("\",\"", columnNames) + "\"");
            foreach(DataRow dr in dt.Rows)
            {
                IEnumerable<string> columnValues = dr.ItemArray.Select(colvalue => colvalue.ToString());
                sb.AppendLine("\"" + string.Join("\",\"", columnValues) + "\"");
            }

            return sb;
        }

        public static void OutputDataTableToAzureDatabase(DataTable dt)
        {

            SqlConnection sqlcnn = new SqlConnection();
            sqlcnn.ConnectionString = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;
            sqlcnn.Open();
            SqlBulkCopy bulkcopy = new SqlBulkCopy(sqlcnn);
            bulkcopy.DestinationTableName = "[MEVO].[ShippingReferenceNumberEntryNumber]";
            bulkcopy.WriteToServer(dt);
            Console.WriteLine("Data imported to Aure Database APP [MEVO].[ShippingReferenceNumberEntryNumber]");

            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = sqlcnn;
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandText = "MEVO.PurgeDuplicateRecords";
            oCmd.ExecuteNonQuery();
            Console.WriteLine("MEVO.PurgeDuplicateRecords executed on Aure Database APP");

            sqlcnn.Close();
            sqlcnn.Dispose();

        }

        public static void ReadInputEntryNumberCsvToDatabase()
        {
            DirectoryInfo d = new DirectoryInfo(ConfigurationManager.AppSettings["InputEntryNumberFolder"]);
            FileInfo[] Files = d.GetFiles("*.csv");
            foreach (FileInfo file in Files)
            {


                string FileName = file.Name;
                string FileNameFullPath = file.FullName;
                Console.WriteLine("Processing {0}", FileNameFullPath);

                StreamReader sr = new StreamReader(FileNameFullPath);
                string line = "";
                string EntryNumber, EntryDate, ClientInvoiceNumber, ShipperRefNumber;
                

                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        string[] arrLine = line.Split(',');

                        if (arrLine.Length == 4)
                        {
                            EntryNumber = arrLine[0];
                            EntryDate = arrLine[1];
                            ClientInvoiceNumber = arrLine[2];
                            ShipperRefNumber = arrLine[3];
                        }
                    }
                }
            }
        }

        public static void SendEmailNotification(string sSubject, string sBody)
        {
            //send out email notification
            #region send out email notification
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("ericlee.omnitrans@gmail.com");
            string sTo = ConfigurationManager.AppSettings["AlertEmailAddress"];
            string[] arrTo = sTo.Split(';');
            foreach (string to in arrTo)
            {
                message.To.Add(new MailAddress(to));
            }

            message.Subject = sSubject;
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = sBody;
            //smtp.Port = 587;
            //smtp.Host = "smtp.gmail.com"; //for gmail host  
            smtp.Host = "mtlmail2.omni.local"; //for gmail host  
            smtp.EnableSsl = true;
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!!");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);

            #endregion
        }
    }
}
